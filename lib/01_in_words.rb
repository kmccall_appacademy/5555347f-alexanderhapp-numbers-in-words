class Fixnum

  def in_words
    numbers_and_words_hash = {
      1_000_000_000_000 => "trillion",
      1_000_000_000 => "billion",
      1_000_000 => "million",
      1000 => "thousand",
      100 => "hundred",
      90 => "ninety",
      80 => "eighty",
      70 => "seventy",
      60 => "sixty",
      50 => "fifty",
      40 => "forty",
      30 => "thirty",
      20 => "twenty",
      19 => "nineteen",
      18 => "eighteen",
      17 => "seventeen",
      16 => "sixteen",
      15 => "fifteen",
      14 => "fourteen",
      13 => "thirteen",
      12 => "twelve",
      11 => "eleven",
      10 => "ten",
      9 => "nine",
      8 => "eight",
      7 => "seven",
      6 => "six",
      5 => "five",
      4 => "four",
      3 => "three",
      2 => "two",
      1 => "one",
      0 => "zero"
    }

    numbers_and_words_hash.each do |num, word|
      if self == num && self <= 20
        return word
      elsif self < 100 && self / num > 0
        return word if self % num == 0
        return "#{word} #{(self % num).in_words}"
      elsif self / num > 0
        return "#{(self / num).in_words} #{word}" if self % num == 0
        return "#{(self / num).in_words} #{word} #{(self % num).in_words}"
      end
    end
  end


end
